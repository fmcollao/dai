/**
 * Created by Francisco Mora on 03-04-2016.
 */
$(function() {
    console.log( "jquery OK..." );

    $( "#formLogin" ).submit(function(evento) {
        evento.preventDefault();
        var regex = /[\w-\.]{1,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
        var url = "mainView.html";


        // Se utiliza la funcion test() nativa de JavaScript
        if (regex.test($('#email').val().trim())) {
            if ( $("#usuario").val() === "administrador" && $("#pwd").val() === "passadministrador123") {
                //alert("Datos OK..");
                $(location).attr('href',url);
            }else{
                alert("Usuario Inválido, Inténtelo nuevamente..");
            }
        } else {
            alert("El email es incorrecto, ingreselo nuevamente..");
        }
    });

    $("#cargarDatos").click(function(){
        /*
         http://ec2-52-38-91-238.us-west-2.compute.amazonaws.com:81/products
         http://ec2-52-38-22-80.us-west-2.compute.amazonaws.com:81/products
         http://52.38.22.80:81/products
         http://ip.jsontest.com/
         http://jsonplaceholder.typicode.com
         http://ec2-52-38-22-80.us-west-2.compute.amazonaws.com:81/products
        */
        $('#tablaProductos').css('display','block');
        var url = "https://query.yahooapis.com/v1/public/yql?q=select%20item.condition%20from%20weather.forecast%20where%20woeid%20in%20%28select%20woeid%20from%20geo.places%281%29%20where%20text%3D%22santo%20domingo%22%29&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
        //var url = "http://ec2-52-38-22-80.us-west-2.compute.amazonaws.com:81/products";
        $.getJSON(url, function( data ) {
            $.each(data, function(i,producto){
                var newRow =
                    "<tr>"
                    +"<td>"+producto.id+"</td>"
                    +"<td>"+producto.name+"</td>"
                    +"<td>"+producto.chipset+"</td>"
                    +"<td>"+producto.colors+"</td>"
                    +"<td>"+producto.gpu+"</td>"
                    +"<td>"+producto.os+"</td>"
                    +"<td>"+producto.sensors+"</td>"
                    +"</tr>";
                $(newRow).appendTo("#grillaProductos");
                /*alert(data);*/
            });
        });
    });
});