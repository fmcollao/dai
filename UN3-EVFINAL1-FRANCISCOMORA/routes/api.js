/**
 * Created by francisco.morac on 28/06/2016.
 */
var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Tareas = mongoose.model('tareas');

router.get('/tareas', function(req, res) {
    Tareas.find(function(err, tareas){
        console.log(tareas);
        res.render(
            'api',
            {titulo : 'Tareas Listado', tareas : tareas}
        );
    });
});

router.post('/tareas', function(req, res) {
    new Tareas({nombre : req.body.nombre})
        .save(function(err, tarea) {
            console.log(tarea);
            res.redirect('/api/tareas');
        });
});

router.get('/tareas/:id', function(req, res) {
    var query = {"_id": req.params.id};
    Tareas.findOne(query, function(err, tarea){
        console.log(tarea);
        res.render(
            'tarea',
            {titulo : '' + tarea.nombre, tarea : tarea}
        );
    });
});

router.put('/tareas/:id', function(req, res) {
    var query = {"_id": req.params.id};
    var update = {nombre : req.body.nombre};
    var options = {new: true};
    Tareas.findOneAndUpdate(query, update, options, function(err, tarea){
        console.log(tarea);
        res.render(
            'tarea',
            {titulo : '' + tarea.nombre, tarea : tarea}
        );
    });
});

router.delete('/tareas/:id', function(req, res) {
    var query = {"_id": req.params.id};
    Tareas.findOneAndRemove(query, function(err, tarea){
        console.log(tarea);
        res.redirect('/api/tareas');
    });
});


module.exports = router;