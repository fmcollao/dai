var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Usuarios = mongoose.model('usuarios');

router.get('/', function (req, res) {
    res.render('login');
});

router.post('/login', function (req, res) {
    var query = {"username": req.body.username};
    Usuarios.findOne(query, function(err, usuario){
        console.log(usuario);
        /*res.render(
            'tarea',
            {titulo : '' + usuario.username}
        );*/
        if (!usuario) {
            res.render('login');
        }else {
            res.redirect('/api/tareas')
        }
    });
});

module.exports = router;