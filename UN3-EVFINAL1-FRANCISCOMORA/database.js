//conectamos a bd con mongoose
var mongoose = require('mongoose');
var tareasSchema   = mongoose.Schema;
var usuarioSchema   = mongoose.Schema;

var Tareas = new tareasSchema(
    {nombre : String}
);

var Usuarios = new usuarioSchema({
    username: {type: String, unique: true},
    password: String
});

mongoose.model('tareas', Tareas);
mongoose.model('usuarios', Usuarios);

mongoose.connect('uri_mongo');
