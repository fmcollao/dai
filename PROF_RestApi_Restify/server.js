/**
 * Created by Francisco on 25-05-2016.
 */
var restify = require('restify');
var mongojs = require('mongojs');

var db = mongojs('mongodb://dai:dai@ds011903.mlab.com:11903/usuariosdai5501', ['productos']);
var server = restify.createServer();
server.use(restify.acceptParser(server.acceptable));
server.use(restify.queryParser());
server.use(restify.bodyParser());

server.listen(3000, function () {
    console.log("Servidor iniciado en puerto 3000");
});

/*
server.get("/product", function (req, resp, next) {
    resp.send("Primera Peticion GET, sobre nuestro servidor");

    return next();
});*/

server.get("/products", function (req, res, next) {
    db.productos.find(function (error, data) {

        res.writeHead(200, {
            'Content Type:' : 'application/json charset=utf-8'
        });

        res.end(JSON.stringify(productos));
    });

    return next();
});

server.post("/products", function (req, res, next) {
    var product = req.params;

    db.productos.save(product, function (err, data) {
        res.writeHead(200, {
            'Content Type:' : 'application/json charset=utf-8'
        });

        res.end(JSON.stringify(productos));
    });

    return next;
});

module.exports = server;