/**
 * Created by Francisco on 25-05-2016.
 */
var restify = require('restify');
var mongojs = require('./server');

var client = restify.createJSONClient({
    url : 'http://localhost:3000'
});

var miProducto = {
    id : '1',
    name : 'Producto 1',
    description : 'Recién insertado',
    por : 'Francisco Mora',
    valor : '99999',
    estado : 'Enviado'
};

client.post('/products', miProducto, function (err, req, res, productos) {

    if(err)
    {
        console.log('ha ocurrido un error al ingresar el producto');
        console.log(err);
    }else{
        console.log('Producto Insertado OK');
        console.log(miProducto);
    }

});