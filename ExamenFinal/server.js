//Inicializando componentes
const express = require('express');
const bodyParser= require('body-parser')
const mongo = require('mongodb').MongoClient;
const app = express();
var bd;

//Configuraciones
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(__dirname + '/public'));
app.use('/bower_components',  express.static(__dirname + '/bower_components'));
app.set('views',__dirname + '/views');
app.set('view engine', 'ejs');

//Routing
app.get('/', (request, response) => {
	console.log(__dirname);
	response.render('pages/cliente/index.ejs');
});

app.get('/login', (request, response) => {
	console.log('Estoy en el login');
	response.render('pages/cliente/login.ejs');
});
app.get('/registro', (request, response) => {
	console.log('Estoy en el registro');
	response.render('pages/cliente/registro.ejs');
});

app.post('/aprobacion', (req, res) => {
	console.log('Enviamos datos a mLab');

	//Se envian datos del formulario de pre-aprobación
	bd.collection('creditos').save(req.body, (err, result) => {
		if (err) {
		return console.log(err);
		}

		console.log('Datos guardados OK');

		res.redirect('/');
	});
});

app.post('/auth', (req, res) => {
	console.log('valido usuario');
	var query = {
		"run": req.body.run,
		"password": req.body.password
	};

	bd.collection('usuarios').findOne(query, function(err, usuario){
		console.log(usuario);
		if (!usuario) {
			console.log('usuario incorrecto');
            res.redirect('/login');
        }else {
        	console.log('usuario autenticado');
            res.redirect('/show');
        }
	});
});

app.post('/nuevouser', (req, res) => {
	console.log('registro nuevo usuario');
	//Se envian datos del formulario de registro de usuario
	bd.collection('usuarios').save(req.body, (err, result) => {
		if (err) {
		return console.log(err);
		}

		console.log('Nuevo usuario guardado OK');

		res.redirect('/login');
	});
});

app.get('/show', (request, response) => {
	console.log('Estoy en el listado de creditos');

	bd.collection('creditos').find().toArray(function(err, creditos){
		if (err) {
			console.log(err);
		} else {
			console.log(creditos);
			response.render('pages/admin/show.ejs', {creditos : creditos});
		}
	});
});

app.get('/show/:id', (request, response) => {
	console.log('veo un credito especifico por _id');
	var query = {"_id" : req.params.id};

	bd.collection('creditos').findOne(query, function(err, credito){
		console.log(credito);
		if (err) {
			console.log(err);
            res.redirect('/ver');
        }else {
        	console.log('usuario autenticado');
            res.redirect('/ver');
        }
	});
});

app.delete('/delete/:id', (request, response) => {
	console.log('elimino credito especifico por _id');
	var query = {"_id" : req.params.id};

	bd.collection('creditos').findOneAndDelete(query, function(err, credito){
		if (err) {
			console.log(err);
        }else {
        	console.log('credito eliminado');
            res.redirect('/show');
        }
	});
});

//Conectando a BD mongo en mLab
mongo.connect('mongodb://examendai:admindai@ds017165.mlab.com:17165/examendai5501', (error, database) => {
	if (error){
		return console.log(error);
	}

	bd = database;
	app.listen(3000, () => {
		console.log('listening on 3000');
	})
});
